import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';

@Component({
  selector: 'app-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.css']
})
export class SidenavComponent {

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(private breakpointObserver: BreakpointObserver) {}

  folders: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  notes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];

  appitems = [
    {
      label: 'Inicio',
      link: '/dashboard',
      icon: 'home'
    },
    // {
    //   label: 'Test',
    //   icon: 'home',
    //   faIcon: 'fab fa-500px',
    //   items: [
    //     {
    //       label: 'Item 1.1',
    //       link: '/item-1-1',
    //       faIcon: 'fab fa-accusoft'
    //     },
    //     {
    //       label: 'Item 1.2',
    //       faIcon: 'fab fa-accessible-icon',
    //       items: [
    //         {
    //           label: 'Item 1.2.1',
    //           link: '/item-1-2-1',
    //           faIcon: 'fas fa-allergies'
    //         },
    //         {
    //           label: 'Item 1.2.2',
    //           faIcon: 'fas fa-ambulance',
    //           items: [
    //             {
    //               label: 'Item 1.2.2.1',
    //               link: 'item-1-2-2-1',
    //               faIcon: 'fas fa-anchor'
    //             }
    //           ]
    //         }
    //       ]
    //     }
    //   ]
    // },
    // {
    //   label: 'Item 2',
    //   icon: 'alarm',
    //   items: [
    //     {
    //       label: 'Item 2.1',
    //       link: '/item-2-1',
    //       icon: 'favorite'
    //     },
    //     {
    //       label: 'Item 2.2',
    //       link: '/item-2-2',
    //       icon: 'favorite_border'
    //     }
    //   ]
    // },
    // {
    //   label: 'Item 3',
    //   link: '/item-3',
    //   icon: 'offline_pin'
    // },
    {
      label: 'Configuracion',
      icon: 'build',
      color:'blue',
      items: [
        {
          label: 'Formas de Pago',
          link: '/configuracion/formapago',
        },
        {
          label: 'Medios de Pago',
          link: '/configuracion/mediopago'
        }
      ]
    }
  ];

  config = {
    paddingAtStart: true,
    classname: 'my-custom-class',
    selectedListFontColor: 'red',
  };
  
}
