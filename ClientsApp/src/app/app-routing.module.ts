import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FormapagoComponent } from './configuracion/formapago/formapago.component';
import { MediopagoComponent } from './configuracion/mediopago/mediopago.component';
import { DashbboardComponent } from './dashboard/dashbboard.component';

const routes: Routes = [
  {path:'dashboard',component:DashbboardComponent},
  {path:'configuracion/formapago',component:FormapagoComponent},
  {path:'configuracion/mediopago',component:MediopagoComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
